# Semantic versioning

Releases of these specifications follow [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

MAJOR releases MAY require the implementors of these specifications to make adjustments (see changelog and release notes for more detail).

MINOR and PATCH releases of these specifications MUST not require changes in:

- Implementations of these specifications.

Changes to any of the following parts MUST be carefully evaluated when determining the semantic version number of the specification release:

- Application endpoints, including path, headers, parameters, body, and their behavior.
- Consumer endpoints, including path, headers, parameters, body, and their behavior.
