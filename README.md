# Specifications

This repository contains the specifications for the redirection flow for consumers to request personal data sharings from individuals.

- [Personal Data Gateway Application OpenAPI specification](openapi.yaml)
- [Personal Data Gateway Consumer OpenAPI specification](consumer/openapi.yaml)

# Redirection flow

```mermaid
sequenceDiagram
    participant Consumer

    box rgba(1,1,1,0.2) Personal Data Gateway
        participant PDG App
    end

    autonumber
    Consumer->>PDG App: Redirect user
    activate PDG App
    Note over PDG App: User authentication
    Note over PDG App: Approve/revoke sharing
    PDG App->>Consumer: Return user
    deactivate PDG App
```
