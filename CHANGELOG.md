# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/compare/v0.1.0...v1.0.0) (2024-05-31)

## [0.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/compare/v0.0.2...v0.1.0) (2024-05-30)


### ⚠ BREAKING CHANGES

* use camel case for query parameter naming
* rename "sourceId" query parameter to "datasourceId" in "GetIndex" operation
* use camel case for query parameter naming

### Features

* rename "sourceId" query parameter to "datasourceId" in "GetIndex" operation ([0101715](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/0101715cc619f345d2e0e7c7d0f9f8bfc57bbbfb))


### Bug Fixes

* add "securitySchemes" object to "components". ([23ae9ff](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/23ae9ffcfdbd428859fd0c851b3e06d9f047d8c6))
* add "securitySchemes" object to "components". ([9a2a08a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/9a2a08a24ee0457d9e32d1eccc6a4ab9267bff56))
* add top-level security scheme ([57bd5ce](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/57bd5ce595907da191949f602e1c6f247315a0f5))
* use camel case for query parameter naming ([9b90de6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/9b90de6a75ea1f33ca0ebe8f5b3ac4114986385a))
* use camel case for query parameter naming ([a6b7bef](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/a6b7bef6aa96d51d28a47334b8da702a8d41c03d))

## 0.0.2 (2024-04-29)


### Features

* add Application OpenAPI specification ([44e9576](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/specs/commit/44e9576ec62be9fe0481d93c6edcb3ba72e57ba1))
